# GNW

GNW ("GNW's Not Windows") is an operating system, licensed under the GNU GPL v3, and with 100% compatibility with Microsoft Windows.

Technical ojectives:

1 - Compatibility with Windows' drivers, executables and DLLs (with backward compatibility for old software & hardware)

2 - Bootable with libre bootloaders (for instance GRUB)

3 - User interface looking like Windows' (for easy transition from Windows)

  3a - Users should be able to select different skins for the UI, looking like their favourite version of Windows

  3b - The taskbar should be in the bottom of the screen

  3c - The start menu should be in the bottom left corner of the screen

  3d - The task manager should be convenient and easy to access

  3e - Same shortcuts as Windows'

4 - Less bugs, more performance, more security

  4a - Community-driven development

  4b - "Release early, release often" development philosophy

5 - Tools (compilers, shell scripts, etc) for easier portability of projects designed for GNU/Linux

6 - Native compatibility with open formats for documents, file systems, etc

Ethical benefits:

1 - Providing Windows' users with an operating system that won't change their habits, but will respect them

2 - Bringing better privacy and security by refusing spyware and backdoors

3 - Putting an end to Microsoft aggressive strategy that forces PC manufacturers to install Windows on the PCs they sell

  3a - Freeing users from the OS' hidden cost of their PC

  3b - Freeing manufacturers from Microsoft's racket

4 - Making sure that old video games will stay compatible forever

5 - Making Windows' users aware of the freedom they can get by refusing proprietary software
